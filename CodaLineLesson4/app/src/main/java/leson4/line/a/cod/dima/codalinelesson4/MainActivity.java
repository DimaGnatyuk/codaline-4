package leson4.line.a.cod.dima.codalinelesson4;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import leson4.line.a.cod.dima.codalinelesson4.adapter.CustomAdapter;
import leson4.line.a.cod.dima.codalinelesson4.entiti.People;

public class MainActivity extends AppCompatActivity  {

    private DrawerLayout mDrawerLayout;
    private View mViewDrawer;
    private ActionBarDrawerToggle mActionBarDrawerToggle;

    private List <People> peoples;
    private TextView textViewName;
    private TextView textViewCountry;
    private TextView textViewSity;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createLink();
        initDrawer();
        initListView();

    }

    private void createLink (){
        textViewName = (TextView) findViewById(R.id.TextName);
        textViewCountry = (TextView) findViewById(R.id.TextCounty);
        textViewSity = (TextView) findViewById(R.id.TextSity);
        imageView = (ImageView) findViewById(R.id.ImageView);
    }

    private void initDrawer() {
        mViewDrawer = findViewById(R.id.view_drawer);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mActionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(mViewDrawer)) {
                    mDrawerLayout.closeDrawer(mViewDrawer);
                } else {
                    mDrawerLayout.openDrawer(mViewDrawer);
                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    private void initListView() {
        ListView listView = (ListView) findViewById(R.id.list_item_people);

        peoples = Arrays.asList(
                new People(1, "Dima1", "Ukraine", "Chercassy", R.drawable.onepeople1),
                new People(2, "Dima2", "Ukraine", "Kiev", R.drawable.onepeople2)
        );

        CustomAdapter adapter = new CustomAdapter(this, peoples);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        loadElementById(position);
                    }
                }
        );
    }

    private void loadElementById (int id){
        People people = peoples.get(id);

        textViewName.setText(people.getName());
        textViewCountry.setText(people.getCountru());
        textViewSity.setText(people.getSity());

        int imgId = people.getImage();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setImageDrawable(getResources().getDrawable(imgId,getBaseContext().getTheme()));
        } else {
            imageView.setImageDrawable(getResources().getDrawable(imgId));
        }
    }
}
