package leson4.line.a.cod.dima.codalinelesson4.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import leson4.line.a.cod.dima.codalinelesson4.R;
import leson4.line.a.cod.dima.codalinelesson4.entiti.People;

public class CustomAdapter extends ArrayAdapter<People> {

    public CustomAdapter(Context context, List<People> objects) {
        super(context, R.layout.item_people, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = View.inflate(getContext(), R.layout.item_people, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        People people = getItem(position);
        holder.textViewName.setText(String.format(people.getName()));

        return convertView;
    }

    private class Holder {
        TextView textViewName;

        public Holder(View rootView) {
            this.textViewName = (TextView) rootView.findViewById(R.id.TextName);
        }
    }
}
