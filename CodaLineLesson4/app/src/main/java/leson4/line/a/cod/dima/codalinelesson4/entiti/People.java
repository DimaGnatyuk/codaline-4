package leson4.line.a.cod.dima.codalinelesson4.entiti;

public class People {

    private int id;
    private String name;
    private String countru;
    private String sity;
    private int image;

    public People (int id, String name, String countru, String sity, int image)
    {
        this.id = id;
        this.name = name;
        this.countru = countru;
        this.sity = sity;
        this.image=image;
    }

    public int getId() {
        return id;
    }

    public String getCountru() {
        return countru;
    }

    public String getName() {
        return name;
    }

    public String getSity() {
        return sity;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

}
